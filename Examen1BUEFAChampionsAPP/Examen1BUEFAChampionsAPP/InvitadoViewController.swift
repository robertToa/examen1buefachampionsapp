//
//  InvitadoViewController.swift
//  Examen1BUEFAChampionsAPP
//
//  Created by Robert on 13/12/17.
//  Copyright © 2017 Robert_Danilo. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
class InvitadoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //mARK:-Variables Globales
    var jsonArray: NSArray?
    var nombreArray: Array<String> = []
    
    //MARK:-Outloet
    @IBOutlet weak var tableView: UITableView!
    
    
    //MARK:-Load
    override func viewDidLoad() {
        super.viewDidLoad()
        llenadoTabla()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:-ACTION
    
    //MARK:-FUNCTION
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.nombreArray.count
    }
    
    
    func llenadoTabla(){
        Alamofire.request("http://api.football-api.com/2.0/competitions?Authorization=565ec012251f932ea4000001fa542ae9d994470e73fdb314a8a56d76") .responseJSON { response in
            if let JSON = response.result.value{
                self.jsonArray = JSON as? NSArray
                for item in self.jsonArray! as! [NSDictionary]{
                    let name = item["name"] as? String
                    self.nombreArray.append((name)!)
                }
                self.tableView.reloadData()
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TablaCell
        cell.NombreLIga.text = self.nombreArray[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Ligas Europeas"
    }

}
