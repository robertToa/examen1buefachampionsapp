//
//  EquiposViewController.swift
//  Examen1BUEFAChampionsAPP
//
//  Created by Robert on 13/12/17.
//  Copyright © 2017 Robert_Danilo. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class EquiposViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK:- VARIABLES GLOBALES
    var jsonArray: NSArray?
    var nombreEquipos: Array<String> = []
    
    //MARK:-OUTLET
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:-LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        llenadoTabla()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    //MARK:-ACTION
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.nombreEquipos.count
    }
    
    
    func llenadoTabla(){
        print("Ingreso para realizar")
        Alamofire.request("http://api.football-api.com/2.0/standings/1005?Authorization=565ec012251f932ea4000001fa542ae9d994470e73fdb314a8a56d76") .responseJSON { response in
            if let JSON = response.result.value{
                self.jsonArray = JSON as? NSArray
                for item in self.jsonArray! as! [NSDictionary]{
                    let name = item["team_name"] as? String
                    self.nombreEquipos.append((name)!)
                    //print(name)
                }
                self.tableView.reloadData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellEquipos", for: indexPath) as! EquiposViewCell
        cell.LabelEquiposCell.text = self.nombreEquipos[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Equipos de la UEFA CHAMPIONS"
    }
    
    

}
