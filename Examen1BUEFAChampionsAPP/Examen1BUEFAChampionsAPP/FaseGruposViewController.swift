//
//  FaseGruposViewController.swift
//  Examen1BUEFAChampionsAPP
//
//  Created by Robert on 13/12/17.
//  Copyright © 2017 Robert_Danilo. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
class FaseGruposViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //MARK:-Variables Globales
    var jsonArray: NSArray?
    var nombreEquipos: Array<String> = []
    var posicionEquipos: Array<String> = []
    var puntosEquipos: Array<String> = []
    var grupoEquipos: Array<String> = []
    //MARK:-Outlet
    
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:-Load
    override func viewDidLoad() {
        super.viewDidLoad()
        llenadoTabla()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:-Action
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.nombreEquipos.count
    }
    
    
    func llenadoTabla(){
        Alamofire.request("http://api.football-api.com/2.0/standings/1005?Authorization=565ec012251f932ea4000001fa542ae9d994470e73fdb314a8a56d76") .responseJSON { response in
            if let JSON = response.result.value{
                self.jsonArray = JSON as? NSArray
                for item in self.jsonArray! as! [NSDictionary]{
                    
                    let name = item["team_name"] as? String
                    let posicion = item["position"] as? String
                    let puntos = item["points"] as? String
                    let grupo = item["comp_group"] as? String
                    self.nombreEquipos.append((name)!)
                    self.posicionEquipos.append((posicion)!)
                    self.puntosEquipos.append((puntos)!)
                     self.grupoEquipos.append((grupo)!)
                    //print(name)
                }
                self.tableView.reloadData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellGrupos", for: indexPath) as! GruposViewCell
        cell.posLabel.text = self.posicionEquipos[indexPath.row]
        cell.posNombre.text = self.nombreEquipos[indexPath.row]
        cell.puntosLabel.text = self.puntosEquipos[indexPath.row]
        cell.grupoLabel.text = self.grupoEquipos[indexPath.row]
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 1...4:
            return "Grupo A"
        case 5...8:
            return "Grupo B"
        case 9...12:
            return "Grupo C"
        case 13...16:
            return "Grupo D"
        case 17...20:
            return "Grupo E"
        case 21...24:
            return "Grupo F"
        case 25...38:
            return "Grupo G"
        case 19...32:
            return "Grupo H"
        default:
            return "Grupo"
        }
    }
    
}
