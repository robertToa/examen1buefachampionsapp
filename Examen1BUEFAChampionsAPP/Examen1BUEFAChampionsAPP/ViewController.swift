//
//  ViewController.swift
//  Examen1BUEFAChampionsAPP
//
//  Created by Robert on 13/12/17.
//  Copyright © 2017 Robert_Danilo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //MARK:-Outlet
    @IBOutlet weak var UsuarioTextfield: UITextField!
    @IBOutlet weak var ContraseniaTextField: UITextField!
    
    //MARK:-Load
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:-Action
    
    @IBAction func LogearseButton(_ sender: Any) {
        let user = UsuarioTextfield.text ?? " "
        let password = ContraseniaTextField.text ?? " "
        switch (user,password) {
        case ("robert","123456"):
            print("login correcto")
            performSegue(withIdentifier: "Principal", sender: self)
        case ("robert",_):
            print("contraseña incorrecta")
            mostrarAlerta(mensaje: "contraseña incorrecta")
        default:
            print("usuario y contraseña incorrecta")
            mostrarAlerta(mensaje: "usuario y contraseña incorrecta")
        }
    }
    
    private func mostrarAlerta(mensaje:String){
        let alertView = UIAlertController(title: "Error", message: mensaje, preferredStyle: .alert)
        let aceptar = UIAlertAction(title: "Aceptar", style: .default) {
            (action) in
            self.UsuarioTextfield.text = ""
            self.ContraseniaTextField.text = ""
        }
        
        alertView.addAction(aceptar)
        
        present(alertView, animated: true, completion: nil)
    }

}

