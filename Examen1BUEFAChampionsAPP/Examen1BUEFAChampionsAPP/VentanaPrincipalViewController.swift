//
//  VentanaPrincipalViewController.swift
//  Examen1BUEFAChampionsAPP
//
//  Created by Robert on 13/12/17.
//  Copyright © 2017 Robert_Danilo. All rights reserved.
//

import UIKit


class VentanaPrincipalViewController: UIViewController {

    //MARK:-Outlet
    //MARK:-Load
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:-Action
    
    @IBAction func EquiposButton(_ sender: Any) {
        performSegue(withIdentifier: "EnlaceEquipos", sender: self)
    }
    
    @IBAction func GruposButton(_ sender: Any) {
        performSegue(withIdentifier: "EnlaceGrupos", sender: self)
    }
    
    @IBAction func JugadoresEquipoButton(_ sender: Any) {
        performSegue(withIdentifier: "EnlaceJugadoresEquipo", sender: self)
    }
    
    @IBAction func InfJugadorButton(_ sender: Any) {
        performSegue(withIdentifier: "EnlaceJugador", sender: self)
    }
}
