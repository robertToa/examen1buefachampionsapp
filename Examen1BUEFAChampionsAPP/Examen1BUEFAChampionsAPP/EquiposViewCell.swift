//
//  EquiposViewCell.swift
//  Examen1BUEFAChampionsAPP
//
//  Created by Robert on 13/12/17.
//  Copyright © 2017 Robert_Danilo. All rights reserved.
//

import UIKit

class EquiposViewCell: UITableViewCell {

    
    //MARK:-OUTLET
    @IBOutlet weak var LabelEquiposCell: UILabel!
    
    //MARK:-LOAD
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
